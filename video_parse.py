import argparse
import os
from re import S
import threading
import subprocess
import logging
from tkinter.messagebox import NO
from typing import IO
import time
import traceback
import cv2
from cv2 import VideoCapture
import numpy as np
logger = logging.getLogger(__file__)
logging.basicConfig(level=logging.DEBUG,
                    format='{asctime} [{levelname:.4}] {name}: {message}', style='{')

FFMPEG_LOG_LEVEL = ["-loglevel", "quiet"]


class VideoSize():
    DEFAULT_WIDTH = 1280
    DEFAULT_HEIGHT = 720
    COMMAND = '-video_size'

    def command(width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        return [VideoSize.COMMAND, f"{width}*{height}"]

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT) -> None:
        self.width = width
        self.height = height

    def __call__(self):
        return VideoSize.command(self.width, self.height)


video_size = VideoSize()


class INPUT_FORMAT_P:
    RAW = [*video_size(),
           "-pix_fmt", "yuv420p", "-f", "rawvideo"]
    WEBM = ["-f", "mpegts"]


class INPUT_FORMAT:
    AUTO = []
    RAW = ["-f", "rawvideo", *video_size(), "-pix_fmt", "yuv420p"]
    WEBM = ["-f", "mpegts"]


class OUTPUT_FORMAT:
    RAW = ["-f", "rawvideo", *video_size(), "-pix_fmt", "yuv420p"]
    WEBM = ["-f", "mpegts", *video_size(), "-pix_fmt", "yuv420p",
            "-acodec", "copy"]
    MP4 = ["-f", "mp4", *video_size(), "-pix_fmt", "yuv420p"]


class FrameFormat():
    def __init__(self, width, height, pixel_size) -> None:
        self.width = width
        self.height = height
        self.pixel_size = pixel_size
        self.frame_size = width * height * pixel_size

    def video_size(self):
        return VideoSize(self.width, self.height)

    def shape(self):
        return (self.height, self.width, self.pixel_size)


class FrameParser():
    def __init__(self, frame_format: FrameFormat) -> None:
        self.buffer = b''
        self.frame_format = frame_format
        self.frame_cnt = 0

    def reset(self):
        self.buffer = b''

    def push(self, data):
        self.buffer += data

    def parse(self):
        frame_size = self.frame_format.frame_size
        if len(self.buffer) < frame_size:
            return False, None
        frame = self.buffer[:frame_size]
        self.buffer = self.buffer[frame_size:]

        frame = np.frombuffer(frame, dtype=np.uint8).reshape(
            self.frame_format.shape())
        self.frame_cnt += 1
        return True, frame


class VideoCaptureFFmpeg():
    def __init__(self) -> None:
        self.parser: FrameParser = None
        pass

    def open(self, command):
        self.command = command
        self.proc = subprocess.Popen(
            command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def reopen(self):
        if self.proc:
            self.proc.terminate()
        
        self.proc = subprocess.Popen(
            self.command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        self.parser.reset()

    def set_parser(self, parser: FrameParser):
        self.parser = parser

    def grab(self):
        data = self.proc.stdout.read(self.parser.frame_format.frame_size)
        self.parser.push(data)

    def retrieve(self):
        return self.parser.parse()
    
    def skip_frame():
        pass

    def read(self):
        while True:
            data = self.proc.stdout.read(self.parser.frame_format.frame_size)
            self.parser.push(data)
            ret, frame = self.parser.parse()
            if ret:
                return True, frame
            else:
                if len(data) == 0:
                    return False, None
                continue
            


# OUTPUT_FORMAT.WEBM -> INPUT_FORMAT.WEBM  OK


# OUTPUT_FORMAT.MP4
"""
[mp4 @ 000001c0b52a8640] muxer does not support non seekable output
Could not write header for output file #0 (incorrect codec parameters ?): Invalid argument
Error initializing output stream 0:0 --
"""


def create_node(command):
    proc = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return proc


def play_node():
    command = ["ffplay", *INPUT_FORMAT_P.WEBM, "-i", "pipe:0"]
    proc = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return proc


def camera_node():
    command = [
        "ffmpeg", '-rtbufsize', '100M', '-f', 'dshow', "-i", 'video=OBS Virtual Camera', *
        OUTPUT_FORMAT.WEBM, "pipe:1"
    ]
    proc = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return proc


def input_node(video_source):
    command = [
        "ffmpeg", *INPUT_FORMAT.AUTO, "-i", video_source, *OUTPUT_FORMAT.WEBM, "pipe:1"
    ]
    proc = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return proc


def output_node(output_file):
    command = [
        "ffmpeg", "-y", *INPUT_FORMAT.WEBM, "-i", "pipe:0", output_file
    ]
    proc = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return proc


def pipe_send(input_pipe: IO, output_pipe: IO, size=None):
    data = input_pipe.read(size)
    output_pipe.write(data)
    logger.debug(f'write {len(data)} byte')
    return len(data)


class OutputPipeDummy(IO):
    def write(self, __s) -> int:
        return 0


def create_pipe(input_pipe: IO, output_pipe: IO, size=None):
    def func():
        try:
            while True:
                if pipe_send(input_pipe, output_pipe, size) == 0:
                    break
            output_pipe.close()
        except Exception as e:
            logger.debug(f'close pipe {traceback.format_exc()}')
    t = threading.Thread(target=func)
    t.setDaemon(True)
    t.start()
    return t


def create_pipe_frame(input_pipe: IO, output_pipe: IO, parser: FrameParser):
    def func():
        try:
            while True:
                data = input_pipe.read(parser.frame_format.frame_size)
                parser.push(data)
                ret, frame = parser.parse()
                if ret:
                    output_pipe.write(frame.tobytes())
                    logger.debug(f'write {len(frame.tobytes())} byte')
                    time.sleep(0.1)
                if len(data) == 0:
                    break
            output_pipe.close()
        except Exception as e:
            logger.debug(f'clone pipe {traceback.format_exc()}')
    t = threading.Thread(target=func)
    t.setDaemon(True)
    t.start()
    return t


# パディング
# ffmpegのsegment-timeオプションが効かないのか
# https://ja.stackoverflow.com/questions/12736/ffmpeg%E3%81%AEsegment-time%E3%82%AA%E3%83%97%E3%82%B7%E3%83%A7%E3%83%B3%E3%81%8C%E5%8A%B9%E3%81%8B%E3%81%AA%E3%81%84%E3%81%AE%E3%81%8B
if __name__ == "__main__":
    #input_video = 'video/2022-06-04 01-09-38.mkv'
    #input_video = 'video/a.webm'
    input_video =  'video/銀杏-VRMJmq1lPIQ.mp4'
    capture = VideoCaptureFFmpeg()

    #frame_format = FrameFormat(1280, 720, 3)
    frame_format = FrameFormat(1920, 1080, 3)
    command = [
        "ffmpeg", *INPUT_FORMAT.AUTO, "-i", input_video, "-f", "rawvideo", *
        frame_format.video_size()(), "-pix_fmt", "bgr24", "pipe:1", *FFMPEG_LOG_LEVEL
    ]
    capture.open(command)
    capture.set_parser(FrameParser(frame_format))
    ret, frame = capture.read()
    #cv2.imshow(None, frame)
    # cv2.waitKey(0)

    folder_name = "hls_pad"
    os.makedirs(folder_name, exist_ok=True)
    command = [
        "ffmpeg", "-f", "rawvideo", *
        frame_format.video_size()(), "-pix_fmt", "bgr24", "-i", "pipe:0", "-f", "hls", "-hls_time", "9", "-hls_playlist_type", "vod", "-hls_segment_filename", f"{folder_name}/video%3d.ts", f"{folder_name}/video.m3u8",
    ]
    hls_encoder = create_node(command)
    capture.reopen()
    for i in range(1200):
        hls_encoder.stdin.write(frame.tobytes())
    hls_encoder.stdin.close()
    hls_encoder.wait()
    hls_encoder.terminate()

    folder_name = "hls_def"
    os.makedirs(folder_name, exist_ok=True)
    command = [
        "ffmpeg", "-f", "rawvideo", *
        frame_format.video_size()(), "-pix_fmt", "bgr24", "-i", "pipe:0", "-f", "hls", "-hls_time", "9", "-hls_playlist_type", "vod", "-hls_segment_filename", f"{folder_name}/video%3d.ts", f"{folder_name}/video.m3u8",
    ]
    hls_encoder = create_node(command)
    capture.reopen()
    for i in range(1200):
        ret, frame = capture.read()
        hls_encoder.stdin.write(frame.tobytes())

    hls_encoder.stdin.close()
    hls_encoder.wait()
    hls_encoder.terminate()

    folder_name = "hls_pad_sparse"
    os.makedirs(folder_name, exist_ok=True)
    command = [
        "ffmpeg", "-f", "rawvideo", *
        frame_format.video_size()(), "-pix_fmt", "bgr24", "-i", "pipe:0", "-f", "hls", "-hls_time", "9", "-hls_playlist_type", "vod", "-hls_segment_filename", f"{folder_name}/video%3d.ts", f"{folder_name}/video.m3u8",
    ]
    hls_encoder = create_node(command)
    capture.reopen()
    for i in range(600):
        ret, frame = capture.read()
        hls_encoder.stdin.write(frame.tobytes())
        hls_encoder.stdin.write(frame.tobytes())

    hls_encoder.stdin.close()
    hls_encoder.wait()
    hls_encoder.terminate()

    folder_name = "hls_pad_ee"
    os.makedirs(folder_name, exist_ok=True)
    command = [
        "ffmpeg", "-f", "rawvideo", *
        frame_format.video_size()(), "-pix_fmt", "bgr24", "-i", "pipe:0", "-f", "hls", "-hls_time", "9", "-hls_playlist_type", "vod", "-hls_segment_filename", f"{folder_name}/video%3d.ts", f"{folder_name}/video.m3u8",
    ]
    hls_encoder = create_node(command)
    capture.reopen()

    ret, frame = capture.read()
    hls_encoder.stdin.write(frame.tobytes())

    for i in range(1200-1):
        ret, frame = capture.read()
    for i in range(1200-1):
        hls_encoder.stdin.write(frame.tobytes())

    hls_encoder.stdin.close()
    hls_encoder.wait()
    hls_encoder.terminate()

    exit()


# ストリーミング
if __name__ == "__main__":
    input_video = 'video/2022-06-04 01-09-38.mkv'

    command = [
        "ffmpeg", *INPUT_FORMAT.AUTO, "-i", input_video, "-f", "rawvideo", *
        video_size(), "-pix_fmt", "bgr24", "pipe:1", *FFMPEG_LOG_LEVEL
    ]
    proc1 = create_node(command)
    command = [
        "ffmpeg", "-f", "rawvideo", *video_size(), "-pix_fmt", "bgr24", "-i", "pipe:0", *
        OUTPUT_FORMAT.WEBM, "pipe:1",
    ]

    # command = ["ffplay","-f", "rawvideo", *video_size(),"-pix_fmt", "bgr24", "-i", "pipe:0"]

    proc2 = create_node(command)
    create_pipe_frame(proc1.stdout, proc2.stdin,
                      FrameParser(FrameFormat(1280, 720, 3)))
    command = [
        "ffmpeg",  *INPUT_FORMAT.WEBM, "-i", "pipe:0", *
        OUTPUT_FORMAT.RAW, "pipe:1", *FFMPEG_LOG_LEVEL
    ]
    # command = ["ffplay","-f", "mpegts", "-pix_fmt", "bgr24", "-i", "pipe:0"]

    proc3 = create_node(command)
    create_pipe(proc2.stdout, proc3.stdin, 1024)
    # create_pipe_frame(proc2.stdout, proc3.stdin, FrameParser(FrameFormat(1280,720,3)))

    command = ["ffplay", *INPUT_FORMAT_P.RAW, "-i", "pipe:0"]
    proc4 = create_node(command)
    create_pipe(proc3.stdout, proc4.stdin,
                FrameFormat(1280, 720, 3).frame_size)

    proc1.wait()
    proc2.wait()
    proc3.wait()
    proc4.wait()


if __name__ == "__main__1":
    input_video = 'video/2022-06-04 01-09-38.mkv'

    # command = [
    #    "ffmpeg","-f","mpegts","-i","pipe:0","-vcodec","h264_nvenc","-acodec","copy","-f","mpegts","pipe:1"
    # ]
    command = [
        "ffmpeg", "-y", "-f", "mpegts", "-i", input_video, "-vcodec", "h264_nvenc", "-acodec", "copy", "-f", "mpegts", "pipe:1"
    ]
    # command = [
    #    "ffmpeg","-i",video_path,"-vcodec","h264_nvenc","-acodec","copy","-f","mpegts","pipe:1"
    # ]

    # ffmpeg -hide_banner -list_devices true -f dshow -i nul
    # command = [
    #   "ffmpeg",'-f', 'dshow',"-i","OBS Virtual Camera","-vcodec","h264_nvenc","-acodec","copy","-f","mpeg2video","pipe:1"
    # ]
    # proc = subprocess.Popen(command,stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    proc1 = input_node(input_video)
    # proc1 = camera_node()

    proc2 = output_node("video/output.mp4")
    # proc2 = play_node()
    # conn_pipe = create_pipe(proc1.stdout,OutputPipeDummy(),1920*1080*3)
    # conn_pipe = create_pipe(proc1.stdout, proc2.stdin, VIDEO_SIZE.DEFAULT_WIDTH*VIDEO_SIZE.DEFAULT_HEIGHT*3)
    conn_pipe = create_pipe(proc1.stdout, proc2.stdin, 1024)

    time.sleep(10)
    proc1.terminate()
    proc1.wait()
    # proc2.terminate()
    proc2.wait()
    conn_pipe.join()
    time.sleep(10)

    # with open(input_video,mode='rb') as f:
    #    proc.stdin.write(f.read())

    # with open('output2.ts',mode='wb') as f:
    #    f.write(proc.stdout.read())

    parser = argparse.ArgumentParser()
    parser.add_argument('--video', type=str, default=8)
    parser.add_argument('--ip', type=str, default='127.0.0.1')
    parser.add_argument('--port', type=int, default=8000)
    args = parser.parse_args()
