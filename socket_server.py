import argparse
import datetime
import socket
import threading
import time

def loop_handler(connection:socket,addr):
    while True:
        try:
            res = connection.recv(4096)
            print(res)
            #送り返す
            connection.send(res)
        except Exception as e:
            connection.close()
            print(f'close {addr[0]}:{addr[1]}')
            break


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--max_conn',type=int,default=8)
    parser.add_argument('--ip',type=str,default='127.0.0.1')
    parser.add_argument('--port',type=int,default=8000)
    args = parser.parse_args()


    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((args.ip, args.port))
    sock.listen(args.max_conn)

    while True:
        try:
            conn, addr = sock.accept()

        except KeyboardInterrupt:
            sock.close()
            exit()
            break
        print(f"connect {addr[0]}:{addr[1]}")

        thread = threading.Thread(target=loop_handler, args=(conn,addr), daemon=True)
        thread.start()