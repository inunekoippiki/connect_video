import argparse
import socket
import threading

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip',type=str,default='127.0.0.1')
    parser.add_argument('--port',type=int,default=8000)
    args = parser.parse_args()

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    sock.connect((args.ip, args.port))

    def Handler(sock):
        while True:
            try:
                read = sock.recv(4096)
                print(f'read:{read.decode()}')
                if (len(read) < 4096) :
                    continue
            except Exception as e:
                continue


    while (True):
        _input = input(">>>")
        if _input == "exit":
            sock.close()
            break
        sock.send(_input.encode("UTF-8"))
        print(f'send: {_input}')
        thread = threading.Thread(target = Handler, args= (sock,), daemon= True)
        thread.start()