import socket
import io
HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 8000              # Arbitrary non-privileged port
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(1)
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        stream = io.BytesIO()
        while True:
            data = conn.recv(1024)
            
            if not data: break
            stream.write(data)
            #conn.sendall(data)
        print(stream.getvalue())