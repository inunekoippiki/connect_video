from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import parse_qs, urlparse
import cgi
address = ('localhost', 8000)
import io

file_data : dict[io.BytesIO] = {}

# https://kazuhira-r.hatenablog.com/entry/2019/08/12/220406
# https://pg-chain.com/python-hex-bin-oct#toc2
# https://developer.mozilla.org/ja/docs/Web/HTTP/Headers/Content-Length
# https://note.nkmk.me/python-bin-oct-hex-int-format/
# https://ja.pymotw.com/2/BaseHTTPServer/

class MyHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        data:io.BytesIO = file_data.get(self.path)
        if data is None:
            self.send_response(404)
            self.end_headers()
            return
            
        print('path = {}'.format(self.path))
       
        self.send_response(200)
        self.send_header('Content-Type', 'hls')
        self.send_header('Content-Length', str(len(data.getvalue())))
        self.end_headers()
        self.wfile.write(data.getvalue())


    def do_POST(self):
        form = cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })

        self.send_response(200)
        self.end_headers()
        self.wfile.write('Client: %s\n' % str(self.client_address))
        self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
        self.wfile.write('Path: %s\n' % self.path)
        self.wfile.write('Form data:\n')
        
        # フォームに POST されたデータの情報を送り返す
        for field in form.keys():
            field_item = form[field]
            if field_item.filename:
                # field はアップロードされたファイルを含みます
                file_data = field_item.file.read()
                file_len = len(file_data)
                del file_data
                self.wfile.write('\tUploaded %s as "%s" (%d bytes)\n' % \
                        (field, field_item.filename, file_len))
            else:
                # 通常のフォーム値
                self.wfile.write('\t%s=%s\n' % (field, form[field].value))
        return

        data = io.BytesIO()
        while True:
            rd = self.rfile.readline()
            l = int(rd[:-2], 16)
            rd = self.rfile.read(l)
            if l == 0:
                break
            self.rfile.readline()
            data.write(rd)

        file_data[self.path] = data

    def do_PUT(self):
        data = io.BytesIO()
        while True:
            rd = self.rfile.readline()
            l = int(rd[:-2], 16)
            rd = self.rfile.read(l)
            if l == 0:
                break
            self.rfile.readline()
            data.write(rd)

        file_data[self.path] = data
        

with HTTPServer(address, MyHTTPRequestHandler) as server:
    server.serve_forever()